//
//  DependentComponentPickerViewController.swift
//  PickerProject
//
//  Created by gio emiliano on 2019-10-09.
//  Copyright © 2019 Giovanne Emiliano. All rights reserved.
//

import UIKit

class DependentComponentPickerViewController: UIViewController,  UIPickerViewDelegate, UIPickerViewDataSource {
    
    
    @IBOutlet weak var dependentPicker: UIPickerView!

    
    private let stateComponet = 0
    private let zipComponent = 0
    private var stateZips = [String: [String]]()
    private var states = [String]()
    private var zips=[String]()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let bundle = Bundle.main
        
        let plistURL = bundle.url(forResource: "stateDictionary", withExtension: "plist")
        stateZips = NSDictionary.init(contentsOf: (plistURL)!) as! [String: [String]]
        let allStates = stateZips.keys
        states = allStates.sorted()
        let selectedState = states[0]
        zips = stateZips[selectedState]!

        // Do any additional setup after loading the view.
    }
    

    
    @IBAction func onButtonPressed(_ sender: Any) {
        
        let stateRow = dependentPicker.selectedRow(inComponent: stateComponet)
        let zipRow = dependentPicker.selectedRow(inComponent: zipComponent)
        
        let state = states[stateRow]
        let zip = zips[zipRow]
        
        
        let title = "You selected Zip code \(zip)!"
        let message = "\(zip) is in state \(state)!"

        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: "ok", style: .default, handler: nil)
        alert.addAction(action)
        present(alert, animated: true, completion: nil)
        
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 2
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if component == stateComponet {
            return states.count
        }
        return zips.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if component == stateComponet {
            return states[row]
        }
        return zips[row]
        
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        // use the row to get the selected row from the picker view
        // using the row extract the value from your datasource (array[row])
        
        if component == stateComponet {
            let selectedState = states[row]
            zips = stateZips[selectedState]!
            dependentPicker.reloadComponent(zipComponent)
            dependentPicker.selectedRow(inComponent: zipComponent)
        }
    }
    
    
    func pickerView(_ pickerView: UIPickerView, widthForComponent component: Int) -> CGFloat {
        let pickerWidth = pickerView.bounds.size.width
        
        if component == zipComponent {
            return pickerWidth/3
        }
        
        return 2 * pickerWidth/3
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
